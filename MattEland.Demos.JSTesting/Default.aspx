﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MattEland.Demos.JSTesting._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

  <div class="jumbotron">
    <h1>JavaScript Unit Testing Demo</h1>
    <p class="lead">Because JavaScript should be tested too</p>
    <button class="btn btn-primary btn-lg" onclick="return handleButtonClick();">Calculate Price</button>
  </div>

</asp:Content>
