﻿/// <reference path="qunit.js" />
/// <reference path="../Logic/MyJSLogic.js" />

QUnit.test( "Calculate Returns 420 when number is 42", 
            function( assert ) {

  // Arrange
  var expected = 420;

  // Act
  var result = calculatePrice(40);

  // Assert
  assert.equal(result, expected);
});

